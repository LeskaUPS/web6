function kalkul() {

  let s = document.getElementsByName("Type");
  let select = s[0];
  let price = 0;
  let prices = getPrices();
  let priceIndex = parseInt(select.value) - 1;
  if (priceIndex >= 0) {
    price = prices.Types[priceIndex];
  }
  
  let radioDiv = document.getElementById("vkus");
  if (select.value == "4")
  { radioDiv.style.display =  "block" ; 

  let vkus = document.getElementsByName("vkusno");
  vkus.forEach(function(radio) {
    if (radio.checked) {
      let optionPrice = prices.vkusno[radio.value];
      if (optionPrice !== undefined) {
        price += optionPrice;
      }
    }
  });
  } else radioDiv.style.display =  "none";

  let checkDiv = document.getElementById("toping");
  if (select.value == "2"||select.value == "3")
  {
	  checkDiv.style.display = "block";

  let toping = document.querySelectorAll("#toping input");
  toping.forEach(function(checkbox) {
    if (checkbox.checked) {
      let topPrice = prices.prodProperties[checkbox.name];
      if (topPrice !== undefined) {
        price += topPrice;
      }
    }
  });	  
  } else
  checkDiv.style.display = "none";
  

  
    let kolvo = document.getElementsByName("kol");
  kolvo.forEach(function(text) {
    if (kolvo!="0") {
        price *= kolvo[0].value;
      }
  });
  
  let pP = document.getElementById("pP");
  pP.innerHTML = price + " рублей";
}

function getPrices() {
  return {
    Types: [80, 40, 60, 20],
    vkusno: {
	  option1: 0,
      option2: 15,
      option3: 51,
    },
    prodProperties: {
      top1: 10,
      top2: 20,
      top3: 30,
      top4: 40,
    }
  };
}

window.addEventListener('DOMContentLoaded', function (event) {
	

  let topingDiv = document.getElementById("toping");
  topingDiv.style.display = "none";
  

  let radioDiv = document.getElementById("vkus");
  radioDiv.style.display = "none";
  

  let s = document.getElementsByName("Type");
  let select = s[0];

  select.addEventListener("change", function(event) {
    let target = event.target;
    console.log(target.value);
    kalkul();
  });
  

  let vkus = document.getElementsByName("vkusno");
  vkus.forEach(function(radio) {
    radio.addEventListener("change", function(event) {
      let r = event.target;
      console.log(r.value);
      kalkul();
    });
  });


  let toping = document.querySelectorAll("#toping input");
  toping.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
      let c = event.target;
      console.log(c.name);
      console.log(c.value);
      kalkul();
    });
  });
kalkul();

});
